ARG GITLAB_BASE_IMAGE=
ARG CI_REGISTRY_IMAGE="registry.gitlab.com/gitlab-org/build/cng"
ARG RUST_TAG="master"

FROM --platform=${TARGETPLATFORM} ${CI_REGISTRY_IMAGE}/gitlab-rust:${RUST_TAG} AS gitlab-rust
FROM --platform=${TARGETPLATFORM} ${GITLAB_BASE_IMAGE} AS builder

COPY --from=gitlab-rust /assets /

ARG BUILD_DIR=/tmp/build
# Requires Ruby 3.1 or newer, because older versions do not support OpenSSL 3.
ARG RUBY_VERSION=3.2.5
ARG JEMALLOC_VERSION=5.3.0

# install development deps
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    libffi-dev \
    libgdbm6 \
    libssl-dev \
    libyaml-dev \
    zlib1g-dev \
    coreutils \
  && rm -rf /var/lib/apt/lists/* \
  && mkdir -p ${BUILD_DIR}

# skip installing gem documentation
RUN mkdir -p /usr/etc && \
    { echo 'install: --no-document';  echo 'update: --no-document'; } >> /usr/etc/gemrc

COPY shared/build-scripts/ /build-scripts
COPY patches/ ${BUILD_DIR}/patches

# Install Ruby from source
RUN buildDeps=' \
  autoconf \
  bison \
  dpkg-dev \
  gcc \
  g++ \
  libbz2-dev \
  libgdbm-dev \
  libglib2.0-dev \
  libncurses-dev \
  libxml2-dev \
  libxslt-dev \
  make \
  xz-utils' \
  && apt-get update \
  && apt-get install -y --no-install-recommends $buildDeps \
  && rm -rf /var/lib/apt/lists/* \
  && cd ${BUILD_DIR} \
  && curl --retry 6 -L -sfo jemalloc.tar.bz2 https://github.com/jemalloc/jemalloc/releases/download/${JEMALLOC_VERSION}/jemalloc-${JEMALLOC_VERSION}.tar.bz2 \
  && tar -xjf jemalloc.tar.bz2 \
  && cd jemalloc-${JEMALLOC_VERSION} \
  && ./autogen.sh --prefix=/usr --enable-prof \
  && make -j "$(nproc)" install \
  && make -j "$(nproc)" install DESTDIR=/assets \
  && cd .. \
  && export RUBY_MAJOR_VERSION="${RUBY_VERSION%.*}" \
  && curl --retry 6 -sfo ruby.tar.xz https://cache.ruby-lang.org/pub/ruby/${RUBY_MAJOR_VERSION}/ruby-${RUBY_VERSION}.tar.xz \
  && tar -xf ruby.tar.xz \
  && rm ruby.tar.xz \
  && cd ruby-${RUBY_VERSION} \
  && /build-scripts/apply_ruby_patches.sh ${BUILD_DIR}/patches ${RUBY_VERSION} \
  && export LDFLAGS="-Wl,--no-as-needed" \
  && cflags="-fno-omit-frame-pointer" ./configure --prefix=/usr --with-jemalloc --with-out-ext=dbm,readline --enable-shared --disable-install-rdoc --disable-install-doc  --without-gmp --without-gdbm --without-tk --disable-dtrace \
  && make -j "$(nproc)" install DESTDIR=/assets \
  && cd \
  && apt-get purge -y --auto-remove $buildDeps \
  && rm -rf ${BUILD_DIR}

## FINAL IMAGE ##

FROM --platform=${TARGETPLATFORM} ${GITLAB_BASE_IMAGE} AS final_builder

# install runtime deps
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    bzip2 \
    libffi-dev \
    libgdbm6 \
    libssl-dev \
    libyaml-dev \
    zlib1g-dev \
    coreutils \
    gnupg2 \
  && rm -rf /var/lib/apt/lists/*

COPY shared/build-scripts/ /build-scripts
COPY --from=builder /assets /
COPY --from=builder /usr/etc/gemrc /usr/etc/gemrc

ARG RUBYGEMS_VERSION
ARG BUNDLER_VERSION
ARG RBREADLINE_VERSION=0.5.5

RUN gem update --no-document --system "$RUBYGEMS_VERSION" \
  && gem install bundler --version "$BUNDLER_VERSION" --force --no-document \
  && curl --retry 6 -sfL https://github.com/connoratherton/rb-readline/archive/v${RBREADLINE_VERSION}.tar.gz | tar -xz \
  && ruby rb-readline-${RBREADLINE_VERSION}/setup.rb && rm -rf rb-readline-${RBREADLINE_VERSION} \
	&& rm -rf /root/.gem/ \
	&& RUBY_VERSION=${RUBY_VERSION} /build-scripts/cleanup-gems /usr/lib/ruby/gems

CMD [ "irb" ]
