#!/usr/bin/env bash

set -uo pipefail

TEST_TAG="${1}"
TEST_REGISTRY='registry.gitlab.com/gitlab-org/build/cng'

mkdir -p dev/exiftool/data
mkdir -p dev/exiftool/output
rm dev/exiftool/data/*
rm dev/exiftool/output/*

download_test_files(){
  local _testdata="${1}"

  BASE_URL="https://gitlab.com/gitlab-org/gitlab/-/raw/master/"

  FILES=$(curl -s "https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab/repository/tree?path=${_testdata}" | jq -r '.[].name')

  # Download each file
  while IFS= read -r file; do
      wget -qP ./dev/exiftool/data "$BASE_URL/${_testdata}/$file"
#      echo "Downloaded $file"
  done <<< "$FILES"
}

run_exiftool() {
  local _image="${1}"; shift
  local _cmd="${*}"

# DEBUG-ONLY - This breaks Test 4
#  echo "  Image: ${TEST_REGISTRY}/${_image}:${TEST_TAG}"
#  echo "Command: exiftool ${_cmd}"
#  echo "   Data: ${PWD}/dev/exiftool/data"

  docker run --rm -i --entrypoint /bin/bash \
    -v "${PWD}/dev/exiftool/data:/data" \
    -v "${PWD}/dev/exiftool/output:/output" \
    "${TEST_REGISTRY}/${_image}:${TEST_TAG}" \
    -c "exiftool ${_cmd}"
}


echo "TEST 1: exiftool command exists in the path and has the expected version"
for _image in 'gitlab-workhorse-ee' 'gitlab-rails-ee'; do
  _VERSION=$(run_exiftool "${_image}" '-ver')
  _STATUS="$?"

  echo "    Image: ${_image}"
  echo "   Status: ${_STATUS}"    # Expect `0`
  echo "  Version: ${_VERSION}"   # Expect `12.68`
done

# Test 2: exiftool command works for Workhorse Exif cleaner
#
# Reference:
#   - https://gitlab.com/gitlab-org/gitlab/-/blob/master/workhorse/internal/upload/exif/exif.go
#   - https://gitlab.com/gitlab-org/gitlab/-/blob/master/workhorse/internal/upload/exif/exif_test.go
echo "TEST 2: exiftool command works for Workhorse Exif cleaner"
download_test_files "workhorse/internal/upload/exif/testdata"
valid_files=("sample_exif.jpg" "sample_exif.tiff")
for _sample in "${valid_files[@]}"; do
  _OUTPUT=$(run_exiftool 'gitlab-workhorse-ee' \
    '-all=' '--IPTC:all' '--XMP-iptcExt:all' '-tagsFromFile' '@' \
    '-ResolutionUnit' '-XResolution' '-YResolution' '-YCbCrSubSampling' \
    '-YCbCrPositioning' '-BitsPerSample' '-ImageHeight' '-ImageWidth' \
    '-ImageSize' '-Copyright' '-CopyrightNotice' '-Orientation' "- < data/${_sample} > output/${_sample}"
  )
  _STATUS="$?"

  echo "   Sample: ${_sample}"
  echo "   Status: ${_STATUS}"    # Expect `0`
done

stat -f "%N: %z" dev/exiftool/output/*         # Expect: `output/workhorse-sample_exif.jpg: 25399`

# Test 3: exiftool removes tags for Rails Exif Sanitizer
#
# Reference:
#   - https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/sanitizers/exif.rb
#   - https://gitlab.com/gitlab-org/gitlab/-/blob/master/spec/lib/gitlab/sanitizers/exif_spec.rb
#
# Test Data:
#   - https://gitlab.com/gitlab-org/gitlab/-/blob/master/spec/fixtures/rails_sample.jpg
echo "TEST 3: exiftool removes tags for Rails Exif Sanitizer"
wget -qP ./dev/exiftool/data "https://gitlab.com/gitlab-org/gitlab/-/raw/master/spec/fixtures/rails_sample.jpg"
_OUTPUT=$(run_exiftool 'gitlab-rails-ee' \
  '-all=' '-tagsFromFile' '@' '--IPTC:all' '--XMP-iptcExt:all' \
  '-ResolutionUnit' '-XResolution' '-YResolution' '-YCbCrSubSampling' \
  '-YCbCrPositioning' '-BitsPerSample' '-ImageHeight' '-ImageWidth' \
  "- < data/rails_sample.jpg > output/rails_sample.jpg"
)
_STATUS="$?"

echo "   Sample: rails_sample.jpg"
echo "   Status: ${_STATUS}"    # Expect `0`

stat -f "%N: %z" dev/exiftool/data/*           # Expect: `data/rails_sample.jpg: 35373`
                                               #         `data/rails_sample.jpg: 35255`

# Test 4: exiftool prints tags for Rails Exif Sanitizer
#
# Reference: Same as Test 3
# Test Data: Same as Test 3
wget -qP ./dev/exiftool/data "https://gitlab.com/gitlab-org/gitlab/-/raw/master/spec/fixtures/rails_sample.jpg"
echo "TEST 4: exiftool prints tags for Rails Exif Sanitizer"
_OUTPUT=$(run_exiftool 'gitlab-rails-ee' \
  '-all' '-j' '-sort' '--IPTC:all' '--XMP-iptcExt:all' \
  "- < data/rails_sample.jpg"
)
_STATUS="$?"

echo "   Sample: rails_sample.jpg"
echo "   Status: ${_STATUS}"                       # Expect `0`
echo "    Ouput: $(echo "${_OUTPUT}" | jq '.' )"    # Expect a valid JSON output
