ARG GITLAB_BASE_IMAGE=
ARG UBI_IMAGE=

FROM ${GITLAB_BASE_IMAGE} AS target

FROM ${UBI_IMAGE} AS build

ARG GITLAB_USER=git
ARG UID=1000
ARG GITLAB_DATA=/var/opt/gitlab
ARG DNF_OPTS
ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS_ROOT

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD gitlab-workhorse-ee.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-exiftool.tar.gz ${DNF_INSTALL_ROOT}/

COPY scripts/ ${DNF_INSTALL_ROOT}/scripts/

RUN install -d -o ${UID} -g 0 -m 0770 ${DNF_INSTALL_ROOT}/var/log/gitlab \
    && install -d -o ${UID} -g 0 -m 0770 ${DNF_INSTALL_ROOT}/srv/gitlab/config \
    && chown -R ${UID}:0 ${DNF_INSTALL_ROOT}/srv/gitlab

RUN microdnf ${DNF_OPTS} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 shadow-utils && \
    adduser -m ${GITLAB_USER} -u ${UID} -R ${DNF_INSTALL_ROOT}/

RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 perl-interpreter \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && install -d -o ${UID} -g 0 -m 0770 ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} \
    && install -d -o ${UID} -g 0 -m 0770 ${DNF_INSTALL_ROOT}${GITLAB_DATA} \
    && rm -f ${DNF_INSTALL_ROOT}/var/lib/dnf/history*

## FINAL IMAGE ##

FROM ${GITLAB_BASE_IMAGE} AS final

ARG GITLAB_VERSION
ARG UID=1000
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-workhorse" \
      name="GitLab Workhorse" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Gitlab Workhorse is a smart reverse proxy for GitLab." \
      description="Gitlab Workhorse is a smart reverse proxy for GitLab. It handles large HTTP requests."

ENV LANG=C.UTF-8

COPY --from=build ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

CMD ["/scripts/start-workhorse"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
