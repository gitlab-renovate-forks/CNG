[[_TOC_]]

# Image integrity with cosign

GitLab signs the CNG images so that our users can
[verify integrity of CNG images](https://docs.gitlab.com/charts/installation/verify_cng_images.html).

This process is done with [cosign](https://github.com/sigstore/cosign).

## How it works

This describes when we sign and verify the images:

1. [We sign images right after we build](#we-sign-images-right-after-we-build).
1. [We copy signatures to tagged images](#we-copy-signatures-to-tagged-images).
1. [We reverify signature after tagging](#we-reverify-signature-after-tagging).
1. [We reverify signature before .com and artifact registry sync-back](#we-reverify-signature-before-com-and-artifact-registry-sync-back).

### We sign images right after we build

On every job that builds our images, after having built and pushed the image to the registry,
we immediately call `cosign sign --recursive` on it.

_We currently don't sign UBI / FIPS images triggered as child pipelines. Issue: https://gitlab.com/gitlab-org/gitlab/-/issues/422146_.

### We copy signatures to tagged images

Whenever we tag an image, we use the `cosign copy` mechanism to copy the signed SHA image and its signature
to the tagged image we're creating.

### We reverify signature after tagging

Because some time might have passed in between the moment we signed the original image, and the moment
we tagged an image and copied the signatures, we also call `cosign verify --recursive` right after
we tag the images.

### We reverify signature before .com and artifact registry sync-back

For release purposes, we only build our final images within the [.org](https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/#org) environment. After building
all the final images in [.org](https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/#org), we will sync them to [.com](https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/#production) and artifact registries. However, for the
same reasons as step 3, we also reverify the signatures just before syncing these images to these
final registries.

## Differences between cosign on .com vs .org

In [.org](https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/#org), we use a key pair to sign and verify the images that is only present in this environment:

```sh
cosign sign --recursive --key "${COSIGN_PRIVATE_KEY}" "${IMAGE_REFERENCE}"
cosign verify --key "${COSIGN_PUBLIC_KEY}" "${IMAGE_REFERENCE}"
```

In [.com](https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/#production) we use GitLab.com OIDC:

```sh
cosign sign --recursive ${IMAGE_REFERENCE}
cosign verify "${IMAGE_REFERENCE}" --certificate-identity "${certificate_identity}" --certificate-oidc-issuer "https://gitlab.com"
```

[There's a proposal](https://gitlab.com/gitlab-org/build/CNG/-/issues/638) to convert the [.org](https://handbook.gitlab.com/handbook/engineering/infrastructure/environments/#org) signing method to OIDC keyless signing.
