ARG RAILS_IMAGE=
ARG UID=1000

# Use a composite image to set the permissions properly in the artifacts. This
# is needed due to two limitations with Docker:
# 1. ADD doesn't set permissions for tarballs: https://github.com/moby/moby/issues/35525
# 2. docker cp doesn't preserve UID/GID: https://github.com/moby/moby/issues/41727
FROM ${RAILS_IMAGE} AS composite

ARG UID

ADD gitlab-webservice-ee.tar.gz /assets/
ADD gitlab-python.tar.gz /assets/
ADD gitlab-logger.tar.gz /assets/usr/local/bin

COPY configuration/ /assets/srv/gitlab/config
COPY scripts/ /assets/scripts

RUN chown -R ${UID}:0 /assets/srv/gitlab

## FINAL IMAGE ##

ARG RAILS_IMAGE

FROM ${RAILS_IMAGE} AS final

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID
ARG FIPS_MODE=0

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-webservice" \
      name="GitLab Web Service" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="GitLab Web Service runs the GitLab Rails application with Puma web server." \
      description="GitLab Web Service runs the GitLab Rails application with Puma web server."

COPY --from=composite /assets/ /

ENV GITALY_FEATURE_DEFAULT_ON=1

RUN cd /srv/gitlab \
    && mkdir -p public/uploads \
    && chmod 0700 public/uploads \
    && chown -R ${UID}:0 public/uploads /home/${GITLAB_USER} /var/log/gitlab \
    && chmod -R g=u public/uploads /home/${GITLAB_USER} /var/log/gitlab

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

# Declare /var/log volume after initial log files
# are written to the perms can be fixed
VOLUME /var/log

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck

CMD ["/scripts/process-wrapper"]
