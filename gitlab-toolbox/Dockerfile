ARG CI_REGISTRY_IMAGE="registry.gitlab.com/gitlab-org/build/cng"
ARG TAG="master"
ARG FROM_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-ee"
ARG PYTHON_TAG=3.9.21
ARG GITALY_IMAGE=

FROM --platform=$TARGETPLATFORM ${CI_REGISTRY_IMAGE}/gitlab-python:${PYTHON_TAG} AS python

FROM --platform=$TARGETPLATFORM ${GITALY_IMAGE} AS gitaly

## FINAL IMAGE ##

FROM --platform=$TARGETPLATFORM ${FROM_IMAGE}:${TAG}

ARG TARGETARCH
ARG PYOPENSSL_VERSION
ARG AWSCLI_VERSION="1.37.13"
ARG S3CMD_VERSION="2.4.0"
ARG GSUTIL_VERSION="5.33"
ARG AZCOPY_STATIC_URL="https://azcopyvnext-awgzd8g7aagqhzhe.b02.azurefd.net/releases/release-10.28.0-20250127/azcopy_linux_${TARGETARCH}_10.28.0.tar.gz"

COPY --from=python /usr/local/bin /usr/local/bin/
COPY --from=python /usr/local/lib /usr/local/lib/
COPY --from=python /usr/local/include /usr/local/include/
COPY --from=gitaly  /usr/local/bin/gitaly-backup /usr/local/bin/

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
  gcc \
  ca-certificates \
  jq \
  openssl \
  tar \
  redis-tools \
  && ldconfig \
  && pip3 install --upgrade awscli==${AWSCLI_VERSION} s3cmd==${S3CMD_VERSION} \
  && pip3 install pyOpenSSL==${PYOPENSSL_VERSION} \
  && pip3 install gsutil==${GSUTIL_VERSION} crcmod \
  && pip3 cache purge \
  && find /usr/local/lib/python3.9 -name '__pycache__' -type d -print -exec rm -r {} + \
  && apt-get purge -y --auto-remove gcc \
  && mkdir /tmp/azcopy \
  && curl -sL "${AZCOPY_STATIC_URL}" | tar xzf - -C /tmp/azcopy --strip-components=1 \
  && cp /tmp/azcopy/azcopy /usr/local/bin && chmod 755 /usr/local/bin/azcopy \
  && rm -rf /tmp/azcopy \
  && rm -rf /var/lib/apt/lists/*

ARG GITLAB_USER=git

COPY scripts/bin/* /usr/local/bin/
COPY scripts/lib/* /usr/lib/ruby/vendor_ruby/

USER $GITLAB_USER:$GITLAB_USER

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
